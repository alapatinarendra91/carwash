package com.carwash.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by i5 on 06-10-2018.
 */

public class CommonListModel {
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("vendorname")
    @Expose
    private String vendorname;
    @SerializedName("vendoremail")
    @Expose
    private String vendoremail;
    @SerializedName("vendormobile")
    @Expose
    private String vendormobile;
    @SerializedName("shop_name")
    @Expose
    private String shopName;
    @SerializedName("shop_address")
    @Expose
    private String shopAddress;
    @SerializedName("shop_image")
    @Expose
    private String shopImage;
    @SerializedName("shop_description")
    @Expose
    private String shopDescription;
    @SerializedName("shoplatitude")
    @Expose
    private String shoplatitude;
    @SerializedName("shoplongitude")
    @Expose
    private String shoplongitude;
    private String vendorid;
    private String favourite_id;
    private String assign_serviceid;
    private String service_name;
    private String service_description;
    private String service_image;
    private String service_price;
    private String service_discount;
    private String carpickup_date;
    private String car_delivereddate;
    private String service_pickup;
    private String service_washing;
    private String service_pending;
    private String service_cardelivered;
    private String state_name;
    private String state_id;
    private String shop_ratings;
    private Integer is_favourite;

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getVendorname() {
        return vendorname;
    }

    public void setVendorname(String vendorname) {
        this.vendorname = vendorname;
    }

    public String getVendoremail() {
        return vendoremail;
    }

    public void setVendoremail(String vendoremail) {
        this.vendoremail = vendoremail;
    }

    public String getVendormobile() {
        return vendormobile;
    }

    public void setVendormobile(String vendormobile) {
        this.vendormobile = vendormobile;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public String getShopImage() {
        return shopImage;
    }

    public void setShopImage(String shopImage) {
        this.shopImage = shopImage;
    }

    public String getShopDescription() {
        return shopDescription;
    }

    public void setShopDescription(String shopDescription) {
        this.shopDescription = shopDescription;
    }

    public String getShoplatitude() {
        return shoplatitude;
    }

    public void setShoplatitude(String shoplatitude) {
        this.shoplatitude = shoplatitude;
    }

    public String getShoplongitude() {
        return shoplongitude;
    }

    public void setShoplongitude(String shoplongitude) {
        this.shoplongitude = shoplongitude;
    }

    public String getVendorid() {
        return vendorid;
    }

    public void setVendorid(String vendorid) {
        this.vendorid = vendorid;
    }

    public String getFavourite_id() {
        return favourite_id;
    }

    public void setFavourite_id(String favourite_id) {
        this.favourite_id = favourite_id;
    }

    public String getAssign_serviceid() {
        return assign_serviceid;
    }

    public void setAssign_serviceid(String assign_serviceid) {
        this.assign_serviceid = assign_serviceid;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getService_description() {
        return service_description;
    }

    public void setService_description(String service_description) {
        this.service_description = service_description;
    }

    public String getService_image() {
        return service_image;
    }

    public void setService_image(String service_image) {
        this.service_image = service_image;
    }

    public String getService_price() {
        return service_price;
    }

    public void setService_price(String service_price) {
        this.service_price = service_price;
    }

    public String getService_discount() {
        return service_discount;
    }

    public void setService_discount(String service_discount) {
        this.service_discount = service_discount;
    }

    public String getCarpickup_date() {
        return carpickup_date;
    }

    public void setCarpickup_date(String carpickup_date) {
        this.carpickup_date = carpickup_date;
    }

    public String getCar_delivereddate() {
        return car_delivereddate;
    }

    public void setCar_delivereddate(String car_delivereddate) {
        this.car_delivereddate = car_delivereddate;
    }

    public String getService_pickup() {
        return service_pickup;
    }

    public void setService_pickup(String service_pickup) {
        this.service_pickup = service_pickup;
    }

    public String getService_washing() {
        return service_washing;
    }

    public void setService_washing(String service_washing) {
        this.service_washing = service_washing;
    }

    public String getService_pending() {
        return service_pending;
    }

    public void setService_pending(String service_pending) {
        this.service_pending = service_pending;
    }

    public String getService_cardelivered() {
        return service_cardelivered;
    }

    public void setService_cardelivered(String service_cardelivered) {
        this.service_cardelivered = service_cardelivered;
    }

    public String getState_name() {
        return state_name;
    }

    public void setState_name(String state_name) {
        this.state_name = state_name;
    }

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    public Integer getIs_favourite() {
        return is_favourite;
    }

    public void setIs_favourite(Integer is_favourite) {
        this.is_favourite = is_favourite;
    }

    public String getShop_ratings() {
        return shop_ratings;
    }

    public void setShop_ratings(String shop_ratings) {
        this.shop_ratings = shop_ratings;
    }
}
