package com.carwash.model;

/**
 * Created by i5 on 02-10-2018.
 */

public class RequestModel {
    private String mobileoremail;
    private String password;
    private String email;
    private String name;
    private String mobile;
    private String address;
    private String customerlatitude;
    private String customerlongitude;
    private String distance;
    private String customerid;
    private String otp;
    private String vendorid;
    private String favouriteid;
    private String fname;
    private String lname;
    private String zipcode;
    private String state;
    private String city;
    private String landmark;
    private String searchname;
    private String oldpassword;
    private String newpassword;
    private String assignserviceid;
    private String pickupdatetime;
    private String deliverydatetime;
    private String description;
    private String choosevendorpick;

    public String getMobileoremail() {
        return mobileoremail;
    }

    public void setMobileoremail(String mobileoremail) {
        this.mobileoremail = mobileoremail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCustomerlatitude() {
        return customerlatitude;
    }

    public void setCustomerlatitude(String customerlatitude) {
        this.customerlatitude = customerlatitude;
    }

    public String getCustomerlongitude() {
        return customerlongitude;
    }

    public void setCustomerlongitude(String customerlongitude) {
        this.customerlongitude = customerlongitude;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getVendorid() {
        return vendorid;
    }

    public void setVendorid(String vendorid) {
        this.vendorid = vendorid;
    }

    public String getFavouriteid() {
        return favouriteid;
    }

    public void setFavouriteid(String favouriteid) {
        this.favouriteid = favouriteid;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getSearchname() {
        return searchname;
    }

    public void setSearchname(String searchname) {
        this.searchname = searchname;
    }

    public String getOldpassword() {
        return oldpassword;
    }

    public void setOldpassword(String oldpassword) {
        this.oldpassword = oldpassword;
    }

    public String getNewpassword() {
        return newpassword;
    }

    public void setNewpassword(String newpassword) {
        this.newpassword = newpassword;
    }

    public String getAssignserviceid() {
        return assignserviceid;
    }

    public void setAssignserviceid(String assignserviceid) {
        this.assignserviceid = assignserviceid;
    }

    public String getPickupdatetime() {
        return pickupdatetime;
    }

    public void setPickupdatetime(String pickupdatetime) {
        this.pickupdatetime = pickupdatetime;
    }

    public String getDeliverydatetime() {
        return deliverydatetime;
    }

    public void setDeliverydatetime(String deliverydatetime) {
        this.deliverydatetime = deliverydatetime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getChoosevendorpick() {
        return choosevendorpick;
    }

    public void setChoosevendorpick(String choosevendorpick) {
        this.choosevendorpick = choosevendorpick;
    }
}
