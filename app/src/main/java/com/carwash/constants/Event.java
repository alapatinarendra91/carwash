package com.carwash.constants;

public interface Event {
    int customerlogin = 1;
    int customerregistration = 2;
    int nearshops = 3;
    int forgotpassword = 4;
    int resetpassword = 5;
    int getfavourites = 6;
    int addfavourite = 7;
    int removefavourites = 8;
    int getprofile = 9;
    int customerprofileupdate = 10;
    int searchservices = 11;
    int changepassword = 12;
    int servicesbyvendorid = 13;
    int postorder = 14;
    int getorders = 15;
    int states = 16;
}
