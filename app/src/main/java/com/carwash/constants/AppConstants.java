package com.carwash.constants;

/**
 * Created by i5 on 01-02-2018.
 */

public class AppConstants {


    public static String displayDateFormate = "yyyy-MM-dd";
    public static String serverDateFormate = "yyyy-MM-dd";

    /* For Shared Prefarenses Only*/
    public static String gcmRegId = "gcmRegId";
    public static String PREF_NAME = "IPH_pref";
    public static String PREF_searchWords = "PREF_searchWords";
    public static String PREF_customer_email = "PREF_customer_email";
    public static String PREF_customer_id = "PREF_customer_id";
    public static String PREF_customer_Name = "PREF_customer_Name";
    public static String PREF_firstTimeLogin = "PREF_firstTimeLogin";


    public static int PERMISSION_REQUEST_Location = 100;

    /*  Used foe Intents Only */
    public static String intent_ComingFrom = "intent_ComingFrom";
    public static String intent_loginScreen = "intent_loginScreen";
    public static String intent_changepassword = "intent_changepassword";
    public static String intent_nearShops = "intent_nearShops";
    public static String intent_favorites = "intent_favorites";
    public static String intent_search = "intent_search";
    public static String intent_search_word = "intent_search_word";
    public static String intent_shopId = "intent_shopId";



    /* Api's*/
    public static String BASE_URL = "http://graylogic.net/Nata/NataService.svc/";

    public static String NATA_Guest = BASE_URL + "NATA_Guest?Sno=&GuestID=&GuestName=&GuestImage=&GuestProfession=&TransDate=&Status=&OrderID=&Condition=Bind";
    public static String NATA_Invite_Operation = BASE_URL + "NATA_Invite_Operation?Sno=&Invite_ID=&Invite_Name=&Invite_Profession=&Invite_Image=&OrderID=&Status=&TransDate=&Condition=SelectGrid";
    public static String NATA_Media_Partner_Operations = BASE_URL + "NATA_Media_Partner_Operations?Sno=&Media_ID=&Media_Name=&Media_Image=&OrderID=&Status=&TransDate=&Condition=SelectGrid";
    public static String NATA_Commitee_Images_Operations = BASE_URL + "NATA_Commitee_Images_Operations?Sno=&Commitiee_ID=&Commitiee_Name=&Commitiee_Image=&OrderID=&Status=&TransDate=&Condition=Select";
    public static String GetEvents = "http://balajiflowersusa.com/apta/jsonevents.php?date=";
    public static String jsoneventimages = "http://balajiflowersusa.com/apta/jsoneventimages.php";
}
