package com.carwash.constants;

public class WebServices {

    /**
     *  LIVE URL          LIVE URL                    LIVE URL            LIVE URL
     */

//     public static String BASE_URL = "https://www.youngsinc.com/YIS_APP_REP_SERVICES_PRO/V7/";
//     public static String GetLogin = "https://www.youngsinc.com/YIS_APP_REP_SERVICES_PRO/V7/applogin";

    /**
     * TEST URL     TEST URL            TEST URL             TEST URL                     TEST URL
     */
    public static String BASE_URL = "http://megaroy.com/carservice/Services/";

    public static String customerregistration = BASE_URL + "customerregistration";
    public static String customerlogin = BASE_URL + "customerlogin";
    public static String forgotpassword = BASE_URL + "forgotpassword";
//    public static String nearshops = BASE_URL + "nearshops";
    public static String nearshops = BASE_URL + "nearshopssearch";
    public static String resetpassword = BASE_URL + "resetpassword";
    public static String getfavourites = BASE_URL + "getfavourites";
    public static String addfavourite = BASE_URL + "addfavourite";
    public static String removefavourites = BASE_URL + "removefavourites";
    public static String getprofile = BASE_URL + "getprofile";
    public static String customerprofileupdate = BASE_URL + "customerprofileupdate";
    public static String searchservices = BASE_URL + "searchservices";
    public static String changepassword = BASE_URL + "changepassword";
    public static String servicesbyvendorid = BASE_URL + "servicesbyvendorid";
    public static String postorder = BASE_URL + "postorder";
    public static String getorders = BASE_URL + "getorders";
    public static String states = BASE_URL + "states";
}
