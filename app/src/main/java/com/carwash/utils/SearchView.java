package com.carwash.utils;

import android.app.Dialog;
import android.content.Context;
import android.text.InputType;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.carwash.R;
import com.carwash.constants.AppConstants;
import com.nex3z.flowlayout.FlowLayout;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;

import static android.view.View.GONE;

/**
 * Created by CIS16 on 28-Dec-17.
 */


public class SearchView {

    private static OnSearchListener onSearchListener;

    public static void loadToolBarSearch(final Context mContext) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.view_toolbar_search, null);
        ImageView imgToolBack = (ImageView) view.findViewById(R.id.img_tool_back);
        final android.support.v7.widget.SearchView edtToolSearch = (android.support.v7.widget.SearchView) view.findViewById(R.id.edt_tool_search);
        LinearLayout mainLay = (LinearLayout) view.findViewById(R.id.mainLay);

        ArrayList<String> searchArrayList = new ArrayList<>();
        if (PrefUtil.loadList(mContext, AppConstants.PREF_NAME, AppConstants.PREF_searchWords) != null) {
            searchArrayList.addAll(PrefUtil.loadList(mContext, AppConstants.PREF_NAME, AppConstants.PREF_searchWords));
        } else {
            mainLay.setVisibility(GONE);
        }
        Collections.reverse(searchArrayList);


        edtToolSearch.onActionViewExpanded();
        /*Changing the cursor color*/
        changeCursorColor(edtToolSearch);

        final Dialog toolbarSearchDialog = new Dialog(mContext, R.style.AppTheme);
        toolbarSearchDialog.setContentView(view);
        toolbarSearchDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        toolbarSearchDialog.getWindow().setGravity(Gravity.TOP);
//        toolbarSearchDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        toolbarSearchDialog.setCanceledOnTouchOutside(true);
        toolbarSearchDialog.show();
        toolbarSearchDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        searchArrayList = (searchArrayList != null && searchArrayList.size() > 0) ? searchArrayList : new ArrayList<String>();

        final FlowLayout flowLayout = (FlowLayout) view.findViewById(R.id.flow);


        for (String text : searchArrayList) {
            final TextView textView = buildLabel(text, mContext);
            flowLayout.addView(textView);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onSearchListener.setSearchListener("search", textView.getText().toString());
                    toolbarSearchDialog.dismiss();
                }
            });
        }

        edtToolSearch.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (edtToolSearch.getQuery().length() >= 3) {
                    onSearchListener.setSearchListener("search", edtToolSearch.getQuery().toString());
                    toolbarSearchDialog.dismiss();
                    edtToolSearch.clearFocus();
                    edtToolSearch.onActionViewCollapsed();
                } else
                    CommonUtils.showToast(mContext, "Please enter minimum 3 characters");

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }

        });

        imgToolBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toolbarSearchDialog.dismiss();
//                searchViewItem.collapseActionView();
            }
        });

    }

    /**
     * @param searchViewAndroidActionBar
     */
    public static void changeCursorColor(android.support.v7.widget.SearchView searchViewAndroidActionBar) {

        final AutoCompleteTextView searchTextView = (AutoCompleteTextView) searchViewAndroidActionBar.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        try {
            searchTextView.setHint("Search service");
            searchTextView.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, R.drawable.cursor);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @param text
     * @param mContext
     * @return
     */
    private static TextView buildLabel(String text, Context mContext) {
        TextView textView = new TextView(mContext);
        textView.setText(text);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        textView.setPadding((int) dpToPx(8, mContext), (int) dpToPx(5, mContext), (int) dpToPx(8, mContext), (int) dpToPx(5, mContext));
        textView.setBackgroundResource(R.drawable.recent_searcg_lable_bg);

        return textView;
    }

    /**
     * @param dp
     * @param mContext
     * @return
     */
    private static float dpToPx(float dp, Context mContext) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, mContext.getResources().getDisplayMetrics());
    }

    public void setOnSearchListener(OnSearchListener onSearchListener) {
        this.onSearchListener = onSearchListener;
    }

    public interface OnSearchListener {
        void setSearchListener(String status, String searchString);
    }
}
