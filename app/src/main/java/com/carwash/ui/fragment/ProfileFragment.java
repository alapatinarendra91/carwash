package com.carwash.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.carwash.R;
import com.carwash.constants.AppConstants;
import com.carwash.constants.ErrorModel;
import com.carwash.constants.Event;
import com.carwash.constants.VolleyErrorListener;
import com.carwash.constants.WebServices;
import com.carwash.model.CommonRootListModel;
import com.carwash.model.CommonRootModel;
import com.carwash.model.RequestModel;
import com.carwash.ui.activiy.RegistrationActivity;
import com.carwash.ui.adapter.ServicePointsAdapter;
import com.carwash.utils.CommonUtils;
import com.carwash.utils.PrefUtil;
import com.carwash.validateui.Field;
import com.carwash.validateui.Form;
import com.carwash.validateui.IsEmail;
import com.carwash.validateui.NotEmpty;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import carwash.ext.GsonObjectRequest;
import carwash.ext.RequestManager;

public class ProfileFragment extends CarWashBaseFragment implements View.OnClickListener{
    private View rootView;
    private Context mContext;
    private EditText firstname, lastname, email, phone, address1, town, landmark, zipcode;
    private Button confirm;
    private Spinner statespinn;
    private CommonRootModel mDataModel;
    private CommonRootListModel mStatesModel;
    private ArrayList<String> statesList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        mContext = getActivity();
        initViews();
        setViews();
        return rootView;
    }
    private void initViews() {
        setToolBarHeading("My Profile",true);
        firstname = (EditText) rootView.findViewById(R.id.firstNameEdt);
        lastname = (EditText) rootView.findViewById(R.id.lastNameEdt);
        email = (EditText) rootView.findViewById(R.id.emailEdt);
        phone = (EditText) rootView.findViewById(R.id.phonenumberEdt);
        address1 = (EditText) rootView.findViewById(R.id.address1Edt);
        town = (EditText) rootView.findViewById(R.id.townEdt);
        landmark = (EditText) rootView.findViewById(R.id.landmarkEdt);
        zipcode = (EditText) rootView.findViewById(R.id.zipcodeEdt);
        confirm = (Button) rootView.findViewById(R.id.confirmbtn);
        statespinn = (Spinner) rootView.findViewById(R.id.statespinn);
    }

    private void setViews() {
        confirm.setOnClickListener(this);

        getData(Event.getprofile);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.confirmbtn:
                if (validateUi())
                    getData(Event.customerprofileupdate);
                break;

        }
    }

    /**
     * Validate the fields
     *
     * @return
     */
    public boolean validateUi() {
        Form mForm = new Form(getActivity());
        mForm.addField(Field.using(firstname).validate(NotEmpty.build(mContext)));
        mForm.addField(Field.using(lastname).validate(NotEmpty.build(mContext)));
        mForm.addField(Field.using(phone).validate(NotEmpty.build(mContext)));
        mForm.addField(Field.using(email).validate(NotEmpty.build(mContext)).validate(IsEmail.build(mContext)));
        mForm.addField(Field.using(address1).validate(NotEmpty.build(mContext)));
        mForm.addField(Field.using(town).validate(NotEmpty.build(mContext)));
        mForm.addField(Field.using(landmark).validate(NotEmpty.build(mContext)));
        mForm.addField(Field.using(zipcode).validate(NotEmpty.build(mContext)));

        if (mForm.isValid() && statespinn.getSelectedItemPosition() == 0){
            CommonUtils.showToast(mContext, "Please select state");
            return false;
        }

        if (mForm.isValid() && phone.getText().toString().length() < 10){
            CommonUtils.showToast(mContext, "Please enter valid mobilenumber");
            return false;
        }

        return (mForm.isValid());
    }


    @Override
    public void getData(int actionID) {
        showProgressDialog(getString(R.string.please_wait), false);
        HashMap<String, String> reqHeader = new HashMap<>();
        reqHeader.put("Content-Type", "application/json; charset=utf-8");
        String request = "";
        Map<String,String> params = new HashMap<String, String>();
        switch (actionID) {
            case Event.getprofile:
                request = sendRequest();
                CommonUtils.logPrint("TAG", "request : " + request);
                RequestManager.addRequest(new GsonObjectRequest<CommonRootModel>
                        (WebServices.getprofile, params, request, CommonRootModel.class,
                                new VolleyErrorListener(this, Event.getprofile)) {
                    @Override
                    protected void deliverResponse(CommonRootModel response) {
                        updateUi(true, Event.getprofile, response);
                    }
                });
                 break;

            case Event.states:
                RequestManager.addRequest(new GsonObjectRequest<CommonRootListModel>
                        (WebServices.states, params, null, CommonRootListModel.class,
                                new VolleyErrorListener(this, Event.states)) {
                    @Override
                    protected void deliverResponse(CommonRootListModel response) {
                        updateUi(true, Event.states, response);
                    }
                });
                break;

            case Event.customerprofileupdate:
                request = sendUpdateRequest();
                CommonUtils.logPrint("TAG", "request : " + request);
                RequestManager.addRequest(new GsonObjectRequest<CommonRootModel>
                        (WebServices.customerprofileupdate, params, request, CommonRootModel.class,
                                new VolleyErrorListener(this, Event.customerprofileupdate)) {
                    @Override
                    protected void deliverResponse(CommonRootModel response) {
                        updateUi(true, Event.customerprofileupdate, response);
                    }
                });
                 break;

        }
    }

    @Override
    public void updateUi(boolean status, int action, Object serviceResponse) {
        removeProgressDialog();
        if (status) {
            switch (action) {
                case Event.getprofile:
                    mDataModel = (CommonRootModel) serviceResponse;
                    getData(Event.states);
                    bindData();
                    break;

                case Event.states:
                    mStatesModel = (CommonRootListModel) serviceResponse;
                    statesList.add("Select State");
                    for (int i =0; i < mStatesModel.getData().size(); i++){
                        statesList.add(mStatesModel.getData().get(i).getState_name());
                    }
                    statespinn.setAdapter(new ArrayAdapter<>(mContext, android.R.layout.simple_spinner_dropdown_item,statesList));

                    for (int i =0; i < mStatesModel.getData().size(); i++){
                        if (mStatesModel.getData().get(i).getState_id().equalsIgnoreCase(mDataModel.getData().getCustomer_state())){
                            statespinn.setSelection(i+1);
                            break;
                        }

                    }
                    break;

                case Event.customerprofileupdate:
                    CommonRootModel mSuccessModel = (CommonRootModel) serviceResponse;
                    CommonUtils.showToast(mContext, "" + mSuccessModel.getStatusText());
                    break;


            }
        } else {
            ErrorModel response = (ErrorModel) serviceResponse;
            CommonUtils.showToast(mContext, "" + response.getStatusText());

        }
    }


    private String sendRequest() {
        RequestModel mModel = new RequestModel();
        mModel.setCustomerid(PrefUtil.getString(mContext, AppConstants.PREF_customer_id, AppConstants.PREF_NAME));
        return new Gson().toJson(mModel);
    }

    private String sendUpdateRequest() {
        RequestModel mModel = new RequestModel();
        mModel.setCustomerid(PrefUtil.getString(mContext, AppConstants.PREF_customer_id, AppConstants.PREF_NAME));
        mModel.setEmail(email.getText().toString());
        mModel.setFname(firstname.getText().toString());
        mModel.setLname(lastname.getText().toString());
        mModel.setMobile(phone.getText().toString());
        mModel.setAddress(address1.getText().toString());
        mModel.setZipcode(zipcode.getText().toString());
        mModel.setState(""+ mStatesModel.getData().get(statespinn.getSelectedItemPosition() - 1).getState_id());
        mModel.setCity(town.getText().toString());
        mModel.setLandmark(landmark.getText().toString());
        return new Gson().toJson(mModel);
    }

    private void bindData(){

        firstname.setText(mDataModel.getData().getCustomer_fname());
        lastname.setText(mDataModel.getData().getCustomer_lname());
        email.setText(mDataModel.getData().getCustomerEmail());
        phone.setText(mDataModel.getData().getCustomerMobile());
        address1.setText(mDataModel.getData().getCustomerAddress());
        town.setText(mDataModel.getData().getCustomer_city());
        landmark.setText(mDataModel.getData().getCustomer_landmark());
        zipcode.setText(mDataModel.getData().getCustomer_zipcode());
    }

}
