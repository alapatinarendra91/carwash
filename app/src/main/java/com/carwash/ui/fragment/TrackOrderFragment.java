package com.carwash.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.carwash.R;
import com.carwash.constants.AppConstants;
import com.carwash.constants.ErrorModel;
import com.carwash.constants.Event;
import com.carwash.constants.VolleyErrorListener;
import com.carwash.constants.WebServices;
import com.carwash.model.CommonRootListModel;
import com.carwash.model.CommonRootModel;
import com.carwash.model.RequestModel;
import com.carwash.utils.CommonUtils;
import com.carwash.utils.PrefUtil;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import carwash.ext.GsonObjectRequest;
import carwash.ext.RequestManager;

public class TrackOrderFragment extends CarWashBaseFragment{
    private View rootView;
    private Context mContext;
    private TextView textShopName,textLocation,textcontact,textemail,textServiceName,textPrice,textPickupDate_Time,textDeliverDate_Time,txtCarPickedUp,txtWashing,txtPolishing,txtOutofDelivery;
    private CommonRootListModel mDataModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_tracking_order, container, false);
        mContext = getActivity();
        initViews();
        setViews();
        getData(Event.getorders);
        return rootView;
    }


    private void initViews() {
        textShopName = (TextView) rootView.findViewById(R.id.textShopName);
        textLocation = (TextView) rootView.findViewById(R.id.textLocation);
        textcontact = (TextView) rootView.findViewById(R.id.textcontact);
        textemail = (TextView) rootView.findViewById(R.id.textemail);
        textServiceName = (TextView) rootView.findViewById(R.id.textServiceName);
        textPrice = (TextView) rootView.findViewById(R.id.textPrice);
        textPickupDate_Time = (TextView) rootView.findViewById(R.id.textPickupDate_Time);
        textDeliverDate_Time = (TextView) rootView.findViewById(R.id.textDeliverDate_Time);
        txtCarPickedUp = (TextView) rootView.findViewById(R.id.txtCarPickedUp);
        txtWashing = (TextView) rootView.findViewById(R.id.txtWashing);
        txtPolishing = (TextView) rootView.findViewById(R.id.txtPolishing);
        txtOutofDelivery = (TextView) rootView.findViewById(R.id.txtOutofDelivery);
    }

    private void setViews() {

    }

    @Override
    public void getData(int actionID) {
        showProgressDialog(getString(R.string.please_wait), false);
        HashMap<String, String> reqHeader = new HashMap<>();
        reqHeader.put("Content-Type", "application/json; charset=utf-8");
        String request = "";
        Map<String,String> params = new HashMap<String, String>();
        switch (actionID) {
            case Event.getorders:
                request = getordersRequest();
                CommonUtils.logPrint("TAG", "request : " + request);
                RequestManager.addRequest(new GsonObjectRequest<CommonRootListModel>
                        (WebServices.getorders, params, request, CommonRootListModel.class,
                                new VolleyErrorListener(this, Event.getorders)) {
                    @Override
                    protected void deliverResponse(CommonRootListModel response) {
                        updateUi(true, Event.getorders, response);
                    }
                });
                break;

        }
    }

    @Override
    public void updateUi(boolean status, int action, Object serviceResponse) {
        removeProgressDialog();
        if (status) {
            switch (action) {
                case Event.getorders:
                    mDataModel = (CommonRootListModel) serviceResponse;
                    bindData();
                    break;

            }
        } else {
            ErrorModel response = (ErrorModel) serviceResponse;
            CommonUtils.showToast(mContext, "" + response.getStatusText());

        }
    }

    private String getordersRequest() {
        RequestModel mModel = new RequestModel();
        mModel.setCustomerid(PrefUtil.getString(mContext, AppConstants.PREF_customer_id, AppConstants.PREF_NAME));
        return new Gson().toJson(mModel);
    }

    private void bindData(){
        textShopName.setText(mDataModel.getData().get(0).getShopName());
        textLocation.setText(CommonUtils.fromHtml("<b><font color=\"#464544\">" + "Location: " + "</font></b>" + mDataModel.getData().get(0).getShopAddress()));
        textcontact.setText(CommonUtils.fromHtml("<b><font color=\"#464544\">" + "Contact: " + "</font></b>" + mDataModel.getData().get(0).getVendormobile()));
        textemail.setText(CommonUtils.fromHtml("<b><font color=\"#464544\">" + "Email: " + "</font></b>" + mDataModel.getData().get(0).getVendoremail()));
        textServiceName.setText(CommonUtils.fromHtml("<b><font color=\"#464544\">" + "Service: " + "</font></b>" + mDataModel.getData().get(0).getService_name()));
        textPrice.setText(CommonUtils.fromHtml("<b><font color=\"#464544\">" + "Price: " + "</font></b>" + mDataModel.getData().get(0).getService_price()));
        textPickupDate_Time.setText(CommonUtils.fromHtml("<b><font color=\"#464544\">" + "Pickup date & time: " + "</font></b>" + mDataModel.getData().get(0).getCarpickup_date()));
        textDeliverDate_Time.setText(CommonUtils.fromHtml("<b><font color=\"#464544\">" + "Deliver  date & time: " + "</font></b>" + mDataModel.getData().get(0).getCar_delivereddate()));
    }

}
