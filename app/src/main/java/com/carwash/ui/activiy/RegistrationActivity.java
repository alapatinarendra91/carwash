package com.carwash.ui.activiy;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.carwash.R;
import com.carwash.constants.ErrorModel;
import com.carwash.constants.Event;
import com.carwash.constants.VolleyErrorListener;
import com.carwash.constants.WebServices;
import com.carwash.model.CommonModel;
import com.carwash.model.CommonRootModel;
import com.carwash.model.RequestModel;
import com.carwash.utils.CommonUtils;
import com.carwash.validateui.Field;
import com.carwash.validateui.Form;
import com.carwash.validateui.IsEmail;
import com.carwash.validateui.NotEmpty;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import carwash.ext.GsonObjectRequest;
import carwash.ext.RequestManager;

public class RegistrationActivity extends CarWashBaseActivity implements View.OnClickListener {
    private Context mContext;
    private EditText first_nameEdt, last_nameEdt, addressEdt, phone_NumberEdit, emailIdEdit, passwordEdit, confirmPasswordEdit;
    private CheckBox termsCheck;
    private Button btn_register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rigistration);

        mContext = this;
        initViews();
        setViews();


    }

    private void initViews() {
        setToolBarHeading("Registration", true);
        first_nameEdt = (EditText) findViewById(R.id.first_nameEdt);
        last_nameEdt = (EditText) findViewById(R.id.last_nameEdt);
        addressEdt = (EditText) findViewById(R.id.addressEdt);
        phone_NumberEdit = (EditText) findViewById(R.id.phone_NumberEdit);
        emailIdEdit = (EditText) findViewById(R.id.emailIdEdit);
        passwordEdit = (EditText) findViewById(R.id.passwordEdit);
        confirmPasswordEdit = (EditText) findViewById(R.id.confirmPasswordEdit);
        termsCheck = (CheckBox) findViewById(R.id.termsCheck);
        btn_register = (Button) findViewById(R.id.btn_register);
    }

    private void setViews() {
        btn_register.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_register:
                if (validateUi())
                    getData(Event.customerregistration);
                break;
        }
    }

    /**
     * Validate the fields
     *
     * @return
     */
    public boolean validateUi() {
        Form mForm = new Form(RegistrationActivity.this);
        mForm.addField(Field.using(first_nameEdt).validate(NotEmpty.build(mContext)));
        mForm.addField(Field.using(last_nameEdt).validate(NotEmpty.build(mContext)));
//        mForm.addField(Field.using(addressEdt).validate(NotEmpty.build(mContext)));
        mForm.addField(Field.using(phone_NumberEdit).validate(NotEmpty.build(mContext)));
        mForm.addField(Field.using(emailIdEdit).validate(NotEmpty.build(mContext)).validate(IsEmail.build(mContext)));
        mForm.addField(Field.using(passwordEdit).validate(NotEmpty.build(mContext)));
        mForm.addField(Field.using(confirmPasswordEdit).validate(NotEmpty.build(mContext)));

        if (phone_NumberEdit.getText().toString().length() < 10){
            CommonUtils.showToast(mContext, "Please enter valid mobilenumber");
            return false;
        }
        if (!passwordEdit.getText().toString().equals(confirmPasswordEdit.getText().toString())) {
            CommonUtils.showToast(mContext, "Do not match your password and confirm password");
            return false;
        }


        return (mForm.isValid());
    }

    @Override
    public void getData(int actionID) {
        showProgressDialog(getString(R.string.please_wait), false);
        HashMap<String, String> reqHeader = new HashMap<>();
        reqHeader.put("Content-Type", "application/json; charset=utf-8");
        String request = "";
        Map<String,String> params = new HashMap<String, String>();
        switch (actionID) {
            case Event.customerregistration:
                request = sendRegisterRequest();
                CommonUtils.logPrint("TAG", "request : " + request);
                RequestManager.addRequest(new GsonObjectRequest<CommonRootModel>
                        (WebServices.customerregistration, params, request, CommonRootModel.class,
                                new VolleyErrorListener(this, Event.customerregistration)) {
                    @Override
                    protected void deliverResponse(CommonRootModel response) {
                        updateUi(true, Event.customerregistration, response);
                    }
                });
                break;

        }
    }

    @Override
    public void updateUi(boolean status, int action, Object serviceResponse) {
        removeProgressDialog();
        if (status) {
            switch (action) {
                case Event.customerregistration:
                    CommonRootModel mRegisterModel = (CommonRootModel) serviceResponse;
                    CommonUtils.showToast(mContext, "" + mRegisterModel.getStatusText());
                    if (mRegisterModel.getStatusCode() == 200){
                        finish();
                    }
//                    PrefUtil.putString(LoginActivity.this, AppConstants.PREF_loggedIn_UserMail, mLoginModel.getEmail(), AppConstants.PREF_NAME);


                    break;

            }
        } else {
            ErrorModel response = (ErrorModel) serviceResponse;
            CommonUtils.showToast(mContext, "" + response.getStatusText());

        }
    }

    private String sendRegisterRequest() {
        RequestModel mModel = new RequestModel();
        mModel.setFname(first_nameEdt.getText().toString());
        mModel.setLname(last_nameEdt.getText().toString());
        mModel.setEmail(emailIdEdit.getText().toString());
        mModel.setPassword(passwordEdit.getText().toString());
        mModel.setMobile(phone_NumberEdit.getText().toString());
        mModel.setAddress("");
        return new Gson().toJson(mModel);
    }

    private void insertRegisterData() {
        showProgressDialog(getString(R.string.please_wait), false);

        StringRequest strReq = new StringRequest(Request.Method.POST,
                WebServices.customerregistration, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                try {
                    removeProgressDialog();
                    JSONObject responseObj = new JSONObject(response);
                    Log.e("tag","Responce is: " + response.toString());
//                    boolean error = responseObj.getBoolean("error");
//                    String message = responseObj.getString("message");
                } catch (JSONException e) {
                    CommonUtils.showToast(mContext,e.getMessage());
                    removeProgressDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                CommonUtils.showToast(mContext,error.getMessage());            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", emailIdEdit.getText().toString());
                params.put("name", last_nameEdt.getText().toString());
                params.put("password", passwordEdit.getText().toString());
                params.put("mobile", phone_NumberEdit.getText().toString());
                params.put("address", addressEdt.getText().toString());
                Log.e(TAG, "Posting params: " + params.toString());
                return params;
            }
        };
        // Adding request to request queue
        Volley.newRequestQueue(this).add(strReq);
//        AppController.getInstance().addToRequestQueue(strReq);

    }


}
