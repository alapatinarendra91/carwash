package com.carwash.ui.activiy;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.carwash.R;
import com.carwash.constants.AppConstants;
import com.carwash.constants.ErrorModel;
import com.carwash.constants.Event;
import com.carwash.constants.VolleyErrorListener;
import com.carwash.constants.WebServices;
import com.carwash.model.CommonRootModel;
import com.carwash.model.RequestModel;
import com.carwash.utils.CommonUtils;
import com.carwash.utils.PrefUtil;
import com.carwash.validateui.Field;
import com.carwash.validateui.Form;
import com.carwash.validateui.IsEmail;
import com.carwash.validateui.NotEmpty;
import com.google.gson.Gson;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.HashMap;
import java.util.Map;

import carwash.ext.GsonObjectRequest;
import carwash.ext.RequestManager;

public class ChangePasswordActivity extends CarWashBaseActivity implements View.OnClickListener {
    private Context mContext;
    private MaterialEditText currentpasswordEdt;
    private EditText newpasswordEdt,confirnnewpasswordEdt;
    private Button updatePasswordBtn;
    private String comingFrom = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        mContext = this;
        initViews();
        setViews();
    }

    private void initViews() {
        setToolBarHeading("Update Password",true);
        currentpasswordEdt = (MaterialEditText ) findViewById(R.id.currentpasswordEdt);
        newpasswordEdt = (EditText) findViewById(R.id.newpasswordEdt);
        confirnnewpasswordEdt = (EditText) findViewById(R.id.confirnnewpasswordEdt);
        updatePasswordBtn = (Button) findViewById(R.id.updatePasswordBtn);

        comingFrom = getIntent().getStringExtra(AppConstants.intent_ComingFrom);

    }

    private void setViews() {
        updatePasswordBtn.setOnClickListener(this);

            if (comingFrom.equalsIgnoreCase(AppConstants.intent_loginScreen)){
                currentpasswordEdt.setHint("Otp");
            }else {
                currentpasswordEdt.setHint("Current Password");
            }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.updatePasswordBtn:
                if (validateUi())
                    getData(comingFrom.equalsIgnoreCase(AppConstants.intent_loginScreen) ? Event.resetpassword  : Event.changepassword);
                break;
        }

    }

    /**
     * Validate the fields
     *
     * @return
     */
    public boolean validateUi() {
        Form mForm = new Form(ChangePasswordActivity.this);
        mForm.addField(Field.using(currentpasswordEdt).validate(NotEmpty.build(mContext)));
        mForm.addField(Field.using(newpasswordEdt).validate(NotEmpty.build(mContext)));
        mForm.addField(Field.using(confirnnewpasswordEdt).validate(NotEmpty.build(mContext)));

        if (!newpasswordEdt.getText().toString().equals(confirnnewpasswordEdt.getText().toString())) {
            CommonUtils.showToast(mContext, "Do not match your password and confirm password");
            return false;
        }


        return (mForm.isValid());
    }

    @Override
    public void getData(int actionID) {
        showProgressDialog(getString(R.string.please_wait), false);
        HashMap<String, String> reqHeader = new HashMap<>();
        reqHeader.put("Content-Type", "application/json; charset=utf-8");
        String request = "";
        Map<String,String> params = new HashMap<String, String>();
        switch (actionID) {
            case Event.resetpassword:
                request = sendresetpasswordRequest();
                CommonUtils.logPrint("TAG", "request : " + request);
                RequestManager.addRequest(new GsonObjectRequest<CommonRootModel>
                        (WebServices.resetpassword, params, request, CommonRootModel.class,
                                new VolleyErrorListener(this, Event.resetpassword)) {
                    @Override
                    protected void deliverResponse(CommonRootModel response) {
                        updateUi(true, Event.resetpassword, response);
                    }
                });
                break;

            case Event.changepassword:
                request = sendChangepasswordRequest();
                CommonUtils.logPrint("TAG", "request : " + request);
                RequestManager.addRequest(new GsonObjectRequest<CommonRootModel>
                        (WebServices.changepassword, params, request, CommonRootModel.class,
                                new VolleyErrorListener(this, Event.changepassword)) {
                    @Override
                    protected void deliverResponse(CommonRootModel response) {
                        updateUi(true, Event.changepassword, response);
                    }
                });
                break;

        }
    }

    @Override
    public void updateUi(boolean status, int action, Object serviceResponse) {
        removeProgressDialog();
        if (status) {
            switch (action) {
                case Event.resetpassword:
                    CommonRootModel mLoginModel = (CommonRootModel) serviceResponse;
                    CommonUtils.showToast(mContext, "" + mLoginModel.getStatusText());
                    if (mLoginModel.getStatusCode() == 200){
                        PrefUtil.putString(mContext, AppConstants.PREF_customer_id, mLoginModel.getData().getCustomerId(), AppConstants.PREF_NAME);
                        PrefUtil.putString(mContext, AppConstants.PREF_customer_email, mLoginModel.getData().getCustomerEmail(), AppConstants.PREF_NAME);
                        finish();
                    }
                    break;

                case Event.changepassword:
                    CommonRootModel mModel = (CommonRootModel) serviceResponse;
                    CommonUtils.showToast(mContext, "" + mModel.getStatusText());
                        finish();
                    break;

            }
        } else {
            ErrorModel response = (ErrorModel) serviceResponse;
            CommonUtils.showToast(mContext, "" + response.getStatusText());

        }
    }



    private String sendresetpasswordRequest() {
        RequestModel mModel = new RequestModel();
        mModel.setCustomerid(PrefUtil.getString(mContext, AppConstants.PREF_customer_id, AppConstants.PREF_NAME));
        mModel.setOtp(currentpasswordEdt.getText().toString());
        mModel.setPassword(newpasswordEdt.getText().toString());
        return new Gson().toJson(mModel);
    }

    private String sendChangepasswordRequest() {
        RequestModel mModel = new RequestModel();
        mModel.setCustomerid(PrefUtil.getString(mContext, AppConstants.PREF_customer_id, AppConstants.PREF_NAME));
        mModel.setOldpassword(currentpasswordEdt.getText().toString());
        mModel.setNewpassword(newpasswordEdt.getText().toString());
        return new Gson().toJson(mModel);
    }
}
