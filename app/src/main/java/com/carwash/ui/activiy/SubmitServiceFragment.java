package com.carwash.ui.activiy;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.carwash.R;
import com.carwash.constants.AppConstants;
import com.carwash.constants.ErrorModel;
import com.carwash.constants.Event;
import com.carwash.constants.VolleyErrorListener;
import com.carwash.constants.WebServices;
import com.carwash.model.CommonRootListModel;
import com.carwash.model.CommonRootModel;
import com.carwash.model.RequestModel;
import com.carwash.ui.adapter.ServicePointsAdapter;
import com.carwash.ui.fragment.CarWashBaseFragment;
import com.carwash.utils.AlertDialogHelper;
import com.carwash.utils.CommonUtils;
import com.carwash.utils.PrefUtil;
import com.carwash.validateui.Field;
import com.carwash.validateui.Form;
import com.carwash.validateui.IsEmail;
import com.carwash.validateui.NotEmpty;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import carwash.ext.GsonObjectRequest;
import carwash.ext.RequestManager;

public class SubmitServiceFragment extends CarWashBaseFragment implements View.OnClickListener, AlertDialogHelper.AlertDialogListener {
    private View rootView;
    private Context mContext;
    private AlertDialogHelper alertDialog;
    private TextView textShopName,textLocation,textcontact,textemail;
    private Spinner servicespin;
    private EditText priceEdt,pickupDateEdt,pickupTimeEdt,deliveryDateEdt,deliveryTimeEdt;
    private CheckBox pickAndDeliveryCheck,termsCheck;
    private Button submitServiceBtn;
    private Calendar myCalendar = Calendar.getInstance();
    private DatePickerDialog datePickerDialog;
    private String dateDialogfrom = "",shopName = "",shopAddress = "", shopContact = "", shopEmail = "",shopId = "";
    ArrayList<String> servicesList = new ArrayList<>();
    private CommonRootListModel mDataModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_submit_service, container, false);
        mContext = getActivity();
        initViews();
        setViews();
        getData(Event.servicesbyvendorid);
        return rootView;
    }

    private void initViews() {
        setToolBarHeading(getString(R.string.submitService),true);
        alertDialog = new AlertDialogHelper(mContext);
        alertDialog.setOnAlertListener(this);


        textShopName = (TextView) rootView.findViewById(R.id.textShopName);
        textLocation = (TextView) rootView.findViewById(R.id.textLocation);
        textcontact = (TextView) rootView.findViewById(R.id.textcontact);
        textemail = (TextView) rootView.findViewById(R.id.textemail);
        servicespin = (Spinner) rootView.findViewById(R.id.servicespin);
        priceEdt = (EditText) rootView.findViewById(R.id.priceEdt);
        pickupDateEdt = (EditText) rootView.findViewById(R.id.pickupDateEdt);
        pickupTimeEdt = (EditText) rootView.findViewById(R.id.pickupTimeEdt);
        deliveryDateEdt = (EditText) rootView.findViewById(R.id.deliveryDateEdt);
        deliveryTimeEdt = (EditText) rootView.findViewById(R.id.deliveryTimeEdt);
        pickAndDeliveryCheck = (CheckBox) rootView.findViewById(R.id.pickAndDeliveryCheck);
        termsCheck = (CheckBox) rootView.findViewById(R.id.termsCheck);
        submitServiceBtn = (Button) rootView.findViewById(R.id.submitServiceBtn);
    }

    private void setViews() {
        pickupDateEdt.setOnClickListener(this);
        pickupTimeEdt.setOnClickListener(this);
        deliveryDateEdt.setOnClickListener(this);
        deliveryTimeEdt.setOnClickListener(this);
        submitServiceBtn.setOnClickListener(this);

        shopId = getArguments().getString(AppConstants.intent_shopId);

        datePickerDialog = new
                DatePickerDialog(mContext, dateListiner,
                myCalendar.get(Calendar.YEAR),myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH ));

        datePickerDialog.getDatePicker().setMinDate(myCalendar.getTimeInMillis());

        servicespin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {

                if (pos != 0)
                priceEdt.setText(mDataModel.getData().get(pos-1).getService_price());

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.pickupDateEdt:
                dateDialogfrom = "pickupDate";
                datePickerDialog.show();
                break;

            case R.id.pickupTimeEdt:
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//                            labendTimeEdt.setText( selectedHour + ":" + selectedMinute);
                        if (String.valueOf(selectedHour).length() == 1 && String.valueOf(selectedMinute).length() == 1)
                            pickupTimeEdt.setText( "0"+selectedHour + ":0" + selectedMinute);
                        else if (String.valueOf(selectedHour).length() == 1)
                            pickupTimeEdt.setText( "0"+selectedHour + ":" + selectedMinute);
                        else if (String.valueOf(selectedMinute).length() == 1)
                            pickupTimeEdt.setText( selectedHour + ":" +"0"+ selectedMinute);
                        else
                            pickupTimeEdt.setText( selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time

                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
                break;

            case R.id.deliveryDateEdt:
                dateDialogfrom = "deliveryDate";
                datePickerDialog.show();
                break;

            case R.id.deliveryTimeEdt:
                // TODO Auto-generated method stub
                Calendar mEndTime = Calendar.getInstance();
                int mEndhour = mEndTime.get(Calendar.HOUR_OF_DAY);
                int mEndminute = mEndTime.get(Calendar.MINUTE);
                TimePickerDialog mEndmTimePicker;
                mEndmTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//                            labendTimeEdt.setText( selectedHour + ":" + selectedMinute);
                        if (String.valueOf(selectedHour).length() == 1 && String.valueOf(selectedMinute).length() == 1)
                            deliveryTimeEdt.setText( "0"+selectedHour + ":0" + selectedMinute);
                        else if (String.valueOf(selectedHour).length() == 1)
                            deliveryTimeEdt.setText( "0"+selectedHour + ":" + selectedMinute);
                        else if (String.valueOf(selectedMinute).length() == 1)
                            deliveryTimeEdt.setText( selectedHour + ":" +"0"+ selectedMinute);
                        else
                            deliveryTimeEdt.setText( selectedHour + ":" + selectedMinute);
                    }
                }, mEndhour, mEndminute, true);//Yes 24 hour time

                mEndmTimePicker.setTitle("Select Time");
                mEndmTimePicker.show();
                break;

            case R.id.submitServiceBtn:
                if (validateUi())
                    getData(Event.postorder);
                break;

        }
    }

    DatePickerDialog.OnDateSetListener dateListiner =
            new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    myCalendar.add(Calendar.DATE, dayOfMonth);
                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                    if (dateDialogfrom.equalsIgnoreCase("pickupDate"))
                        pickupDateEdt.setText(new SimpleDateFormat(AppConstants.displayDateFormate, Locale.US).format(myCalendar.getTime()));

                    else if (dateDialogfrom.equalsIgnoreCase("deliveryDate"))
                        deliveryDateEdt.setText(new SimpleDateFormat(AppConstants.displayDateFormate, Locale.US).format(myCalendar.getTime()));
                }

            };


    /**
     * Validate the fields
     *
     * @return
     */
    public boolean validateUi() {
        if (servicespin.getSelectedItemPosition() == 0){
            CommonUtils.showToast(mContext, "Please Select Service");
            return false;
        }

        Form mForm = new Form(getActivity());
        mForm.addField(Field.using(pickupDateEdt).validate(NotEmpty.build(mContext)));
        mForm.addField(Field.using(pickupTimeEdt).validate(NotEmpty.build(mContext)));
        mForm.addField(Field.using(deliveryDateEdt).validate(NotEmpty.build(mContext)));
        mForm.addField(Field.using(deliveryTimeEdt).validate(NotEmpty.build(mContext)));

        if (!termsCheck.isChecked()){
            CommonUtils.showToast(mContext, "Please check terms & conditions");
            return false;
        }

        return (mForm.isValid());
    }


    @Override
    public void getData(int actionID) {
        showProgressDialog(getString(R.string.please_wait), false);
        HashMap<String, String> reqHeader = new HashMap<>();
        reqHeader.put("Content-Type", "application/json; charset=utf-8");
        String request = "";
        Map<String,String> params = new HashMap<String, String>();
        switch (actionID) {
            case Event.servicesbyvendorid:
                request = getServicesRequest();
                CommonUtils.logPrint("TAG", "request : " + request);
                RequestManager.addRequest(new GsonObjectRequest<CommonRootListModel>
                        (WebServices.servicesbyvendorid, params, request, CommonRootListModel.class,
                                new VolleyErrorListener(this, Event.servicesbyvendorid)) {
                    @Override
                    protected void deliverResponse(CommonRootListModel response) {
                        updateUi(true, Event.servicesbyvendorid, response);
                    }
                });
                break;

            case Event.postorder:
                request = PostorderRequest();
                CommonUtils.logPrint("TAG", "request : " + request);
                RequestManager.addRequest(new GsonObjectRequest<CommonRootModel>
                        (WebServices.postorder, params, request, CommonRootModel.class,
                                new VolleyErrorListener(this, Event.postorder)) {
                    @Override
                    protected void deliverResponse(CommonRootModel response) {
                        updateUi(true, Event.postorder, response);
                    }
                });
                break;
        }
    }

    @Override
    public void updateUi(boolean status, int action, Object serviceResponse) {
        removeProgressDialog();
        if (status) {
            switch (action) {
                case Event.servicesbyvendorid:
                    mDataModel = (CommonRootListModel) serviceResponse;
                    servicesList.add("Select Service");
                    for (int i =0; i < mDataModel.getData().size(); i++){
                        servicesList.add(mDataModel.getData().get(i).getService_name());
                    }

                    servicespin.setAdapter(new ArrayAdapter<>(mContext, android.R.layout.simple_spinner_dropdown_item,servicesList));
                    textShopName.setText(mDataModel.getData().get(0).getShopName());
                    textLocation.setText(CommonUtils.fromHtml("<b><font color=\"#464544\">" + "Location: " + "</font></b>" + mDataModel.getData().get(0).getShopAddress()));
                    textcontact.setText(CommonUtils.fromHtml("<b><font color=\"#464544\">" + "Contact: " + "</font></b>" + mDataModel.getData().get(0).getVendormobile()));
                    textemail.setText(CommonUtils.fromHtml("<b><font color=\"#464544\">" + "Email: " + "</font></b>" + mDataModel.getData().get(0).getVendoremail()));
                    break;

                case Event.postorder:
                    CommonRootModel mSuccessModel = (CommonRootModel) serviceResponse;
                    CommonUtils.showToast(mContext, "" + mSuccessModel.getStatusText());

                    break;

            }
        } else {
            ErrorModel response = (ErrorModel) serviceResponse;
            CommonUtils.showToast(mContext, "" + response.getStatusText());

        }
    }

    private String getServicesRequest() {
        RequestModel mModel = new RequestModel();
        mModel.setVendorid(shopId);
        return new Gson().toJson(mModel);
    }

    private String PostorderRequest() {
        RequestModel mModel = new RequestModel();
        mModel.setCustomerid(PrefUtil.getString(mContext, AppConstants.PREF_customer_id, AppConstants.PREF_NAME));
        mModel.setAssignserviceid(mDataModel.getData().get(servicespin.getSelectedItemPosition() - 1).getAssign_serviceid());
        mModel.setPickupdatetime(pickupDateEdt.getText().toString() + " " + pickupTimeEdt.getText().toString());
        mModel.setDeliverydatetime(deliveryDateEdt.getText().toString() + " " + deliveryTimeEdt.getText().toString());
        mModel.setDescription("");
        mModel.setChoosevendorpick(pickAndDeliveryCheck.isChecked() ? "1" : "0");
        return new Gson().toJson(mModel);
    }

    @Override
    public void onPositiveClick(int from) {

    }

    @Override
    public void onNegativeClick(int from) {

    }

    @Override
    public void onNeutralClick(int from) {

    }
}
