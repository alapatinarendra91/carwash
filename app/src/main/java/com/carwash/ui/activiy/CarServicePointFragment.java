package com.carwash.ui.activiy;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.carwash.R;
import com.carwash.constants.AppConstants;
import com.carwash.constants.ErrorModel;
import com.carwash.constants.Event;
import com.carwash.constants.VolleyErrorListener;
import com.carwash.constants.WebServices;
import com.carwash.custom.GPSTracker;
import com.carwash.model.CommonRootListModel;
import com.carwash.model.CommonRootModel;
import com.carwash.model.RequestModel;
import com.carwash.ui.adapter.ServicePointsAdapter;
import com.carwash.ui.fragment.CarWashBaseFragment;
import com.carwash.utils.CommonUtils;
import com.carwash.utils.PrefUtil;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import carwash.ext.GsonObjectRequest;
import carwash.ext.RequestManager;
import carwash.utils.ToastUtils;

public class CarServicePointFragment extends CarWashBaseFragment implements ServicePointsAdapter.OnCartChangedListener {
    private View rootView;
    private Context mContext;
    private RecyclerView recyclerView;
    private ServicePointsAdapter adapter;
    private GPSTracker gpsLocation;
    private String comingFrom = "",search_word = "";
    private CommonRootListModel mDataModel,mSuccessModel;
    private int selectedPosition = 0;
    private FragmentTransaction ft;


//    public CarServicePointFragment() {
//        // Required empty public constructor
//    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.activity_car_service_point_fragment, container, false);
        mContext = getActivity();
        initViews();
        setViews();
        return rootView;
    }

    private void initViews() {
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        gpsLocation = new GPSTracker(mContext);
    }

    private void setViews() {
        comingFrom = getArguments().getString(AppConstants.intent_ComingFrom);

        if (comingFrom.equalsIgnoreCase(AppConstants.intent_nearShops))
        getData(Event.nearshops);

        if (comingFrom.equalsIgnoreCase(AppConstants.intent_favorites))
        getData(Event.getfavourites);

        if (comingFrom.equalsIgnoreCase(AppConstants.intent_search)){
            search_word = getArguments().getString(AppConstants.intent_search_word);
            getData(Event.searchservices);
        }


    }


    @Override
    public void  getData(int actionID) {
        showProgressDialog(getString(R.string.please_wait), false);
        HashMap<String, String> reqHeader = new HashMap<>();
        reqHeader.put("Content-Type", "application/json; charset=utf-8");
        String request = "";
        Map<String,String> params = new HashMap<String, String>();
        switch (actionID) {
            case Event.nearshops:
                request = sendRequest();
                CommonUtils.logPrint("TAG", "request : " + request);
                RequestManager.addRequest(new GsonObjectRequest<CommonRootListModel>
                        (WebServices.nearshops, params, request, CommonRootListModel.class,
                                new VolleyErrorListener(this, Event.nearshops)) {
                    @Override
                    protected void deliverResponse(CommonRootListModel response) {
                        updateUi(true, Event.nearshops, response);
                    }
                });
                break;

            case Event.getfavourites:
                request = sendGetfavouritesRequest();
                CommonUtils.logPrint("TAG", "request : " + request);
                RequestManager.addRequest(new GsonObjectRequest<CommonRootListModel>
                        (WebServices.getfavourites, params, request, CommonRootListModel.class,
                                new VolleyErrorListener(this, Event.getfavourites)) {
                    @Override
                    protected void deliverResponse(CommonRootListModel response) {
                        updateUi(true, Event.getfavourites, response);
                    }
                });
                break;

            case Event.searchservices:
                request = sendSearchservicesRequest();
                CommonUtils.logPrint("TAG", "request : " + request);
                RequestManager.addRequest(new GsonObjectRequest<CommonRootListModel>
                        (WebServices.searchservices, params, request, CommonRootListModel.class,
                                new VolleyErrorListener(this, Event.searchservices)) {
                    @Override
                    protected void deliverResponse(CommonRootListModel response) {
                        updateUi(true, Event.searchservices, response);
                    }
                });
                break;

            case Event.addfavourite:
                request = sendAddfavouriteRequest();
                CommonUtils.logPrint("TAG", "request : " + request);
                RequestManager.addRequest(new GsonObjectRequest<CommonRootListModel>
                        (WebServices.addfavourite, params, request, CommonRootListModel.class,
                                new VolleyErrorListener(this, Event.addfavourite)) {
                    @Override
                    protected void deliverResponse(CommonRootListModel response) {
                        updateUi(true, Event.addfavourite, response);
                    }
                });
                break;

            case Event.removefavourites:
                request = sendRemovefavouriteRequest();
                CommonUtils.logPrint("TAG", "request : " + request);
                RequestManager.addRequest(new GsonObjectRequest<CommonRootListModel>
                        (WebServices.removefavourites, params, request, CommonRootListModel.class,
                                new VolleyErrorListener(this, Event.removefavourites)) {
                    @Override
                    protected void deliverResponse(CommonRootListModel response) {
                        updateUi(true, Event.removefavourites, response);
                    }
                });
                break;

        }
    }

    @Override
    public void updateUi(boolean status, int action, Object serviceResponse) {
        removeProgressDialog();
        if (status) {
            switch (action) {
                case Event.nearshops:
                case Event.getfavourites:
                case Event.searchservices:
                    mDataModel = (CommonRootListModel) serviceResponse;
                        adapter = new ServicePointsAdapter(mContext,mDataModel.getData());
                        recyclerView.setAdapter(adapter);
                        adapter.setOnCartChangedListener(this);
                    break;

                case Event.removefavourites:
                    mSuccessModel = (CommonRootListModel) serviceResponse;
                    CommonUtils.showToast(mContext, "" + mSuccessModel.getStatusText());
                    mDataModel.getData().get(selectedPosition).setIs_favourite(0);
                    adapter.notifyDataSetChanged();
                    break;

                case Event.addfavourite:
                    mSuccessModel = (CommonRootListModel) serviceResponse;
                    CommonUtils.showToast(mContext, "" + mSuccessModel.getStatusText());
                    mDataModel.getData().get(selectedPosition).setIs_favourite(1);
                    adapter.notifyDataSetChanged();
                    break;

            }
        } else {
            ErrorModel response = (ErrorModel) serviceResponse;
            CommonUtils.showToast(mContext, "" + response.getStatusText());

        }
    }



    private String sendRequest() {
        RequestModel mModel = new RequestModel();
        mModel.setCustomerlatitude("17.5555555");
        mModel.setCustomerlongitude("77.555555555");
        mModel.setDistance("100");
        mModel.setCustomerid(PrefUtil.getString(mContext, AppConstants.PREF_customer_id, AppConstants.PREF_NAME));
        return new Gson().toJson(mModel);
    }

   private String sendSearchservicesRequest() {
        RequestModel mModel = new RequestModel();
        mModel.setSearchname(search_word);
        mModel.setCustomerid(PrefUtil.getString(mContext, AppConstants.PREF_customer_id, AppConstants.PREF_NAME));
        return new Gson().toJson(mModel);
    }

    private String sendGetfavouritesRequest() {
        RequestModel mModel = new RequestModel();
        mModel.setCustomerid(PrefUtil.getString(mContext, AppConstants.PREF_customer_id, AppConstants.PREF_NAME));
        return new Gson().toJson(mModel);
    }

    private String sendAddfavouriteRequest() {
        RequestModel mModel = new RequestModel();
        mModel.setCustomerid(PrefUtil.getString(mContext, AppConstants.PREF_customer_id, AppConstants.PREF_NAME));
        mModel.setVendorid(mDataModel.getData().get(selectedPosition).getVendorid());
        return new Gson().toJson(mModel);
    }

    private String sendRemovefavouriteRequest() {
        RequestModel mModel = new RequestModel();
        mModel.setFavouriteid(mDataModel.getData().get(selectedPosition).getFavourite_id());
        return new Gson().toJson(mModel);
    }

    @Override
    public void setCartClickListener(String status, int position) {
        selectedPosition = position;

        if (status.equalsIgnoreCase("imagefav")){
            getData(mDataModel.getData().get(selectedPosition).getIs_favourite() != null && mDataModel.getData().get(selectedPosition).getIs_favourite() == 1 ?Event.removefavourites : Event.addfavourite);
//            getData(Event.removefavourites);
        }else if (status.equalsIgnoreCase("convertView")){
            Bundle bundle = new Bundle();
            bundle.putString(AppConstants.intent_shopId, mDataModel.getData().get(selectedPosition).getVendorid());
            SubmitServiceFragment mFragment = new SubmitServiceFragment();
            mFragment.setArguments(bundle);

            ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.container, mFragment);
            ft.addToBackStack("");
            ft.commit();
        }
    }
}

