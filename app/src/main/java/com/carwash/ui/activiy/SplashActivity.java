package com.carwash.ui.activiy;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.carwash.MainActivity;
import com.carwash.R;
import com.carwash.constants.AppConstants;
import com.carwash.utils.PrefUtil;

public class SplashActivity extends Activity {

    /**
     * Called when the activity is first created.
     */
    private Context mContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mContext = this;

        /**
         *  App always open resuming background state
         */
        if (!isTaskRoot()) {
            finish();
            return;
        }

        /**
         *  This Method Shows the our logo in 3 seconds
         */
        splash();

    }
    private void splash() {
        final boolean _active = true;
        final int _splashTime = 3000;
        Thread splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    while (_active && (waited < _splashTime)) {
                        sleep(100);
                        if (_active) {
                            waited += 100;
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    if (PrefUtil.getBool(mContext, AppConstants.PREF_firstTimeLogin, AppConstants.PREF_NAME) == false)
                    startActivity(new Intent(mContext, LoginActivity.class));
                    else
                        startActivity(new Intent(mContext, HomeActivity.class));

                    finish();
                }
            }
        };
        splashTread.start();
    }
}

