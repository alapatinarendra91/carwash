package com.carwash.ui.activiy;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.bottomnavigation.LabelVisibilityMode;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.MenuItem;

import com.carwash.R;
import com.carwash.constants.AppConstants;
import com.carwash.ui.fragment.ProfileFragment;
import com.carwash.ui.fragment.TrackOrderFragment;
import com.carwash.utils.AlertDialogHelper;
import com.carwash.utils.CommonUtils;
import com.carwash.utils.SearchView;

import carwash.ui.activity.OrdersActivity;

import static android.support.design.bottomnavigation.LabelVisibilityMode.*;

public class HomeActivity extends CarWashBaseActivity implements AlertDialogHelper.AlertDialogListener,SearchView.OnSearchListener  {
    private Context mContext;
    private BottomNavigationView bottom_navigation;
    private AlertDialogHelper alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initViews();
        setviews();
    }

    private void initViews() {
        mContext = this;
        setToolBarHeading(getString(R.string.home));
        alertDialog = new AlertDialogHelper(mContext);
        alertDialog.setOnAlertListener(this);
        bottom_navigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);


        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.intent_ComingFrom, AppConstants.intent_nearShops);
        CarServicePointFragment mFragment = new CarServicePointFragment();
        mFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, mFragment).commit();
    }

    private void setviews() {
        bottom_navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                // handle desired action here
                if (item.getItemId() == R.id.home){
                    gooCerviceCenter(AppConstants.intent_nearShops);
                    setToolBarHeading(getString(R.string.home));
//                    Bundle bundle = new Bundle();
//                    bundle.putString(AppConstants.intent_ComingFrom, AppConstants.intent_nearShops);
//                    CarServicePointFragment mFragment = new CarServicePointFragment();
//                    mFragment.setArguments(bundle);
//                    getSupportFragmentManager().beginTransaction().replace(R.id.container, mFragment).commit();


                }else if (item.getItemId() == R.id.serviceCenter) {
                    gooCerviceCenter(AppConstants.intent_nearShops);
                    setToolBarHeading(getString(R.string.quickBooking));

                }else if (item.getItemId() == R.id.wishList) {
                    setToolBarHeading(getString(R.string.wishList));
                    gooCerviceCenter(AppConstants.intent_favorites);
//                    Bundle bundle = new Bundle();
//                    bundle.putString(AppConstants.intent_ComingFrom, AppConstants.intent_favorites);
//                    CarServicePointFragment mFragment = new CarServicePointFragment();
//                    mFragment.setArguments(bundle);
//                    getSupportFragmentManager().beginTransaction().replace(R.id.container, mFragment).commit();

                } else if (item.getItemId() == R.id.orders) {
//                    startActivity(new Intent(mContext, OrdersActivity.class));
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, new TrackOrderFragment()).commit();
                    setToolBarHeading(getString(R.string.tracking));

                }else if (item.getItemId() == R.id.account){
                    replaceFragment(new AccountFragment(), null, true, true, false, null, R.id.container);
//                    getSupportFragmentManager().beginTransaction().replace(R.id.container, new AccountFragment()).commit();
                    setToolBarHeading(getString(R.string.account));
                }
                return true;
            }
        });
    }

    private void gooCerviceCenter(String comingFrom){
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.intent_ComingFrom, comingFrom);
        CarServicePointFragment mFragment = new CarServicePointFragment();
        mFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, mFragment).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                SearchView mSearchView = new SearchView();
                mSearchView.loadToolBarSearch(mContext);
                mSearchView.setOnSearchListener(this);
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPositiveClick(int from) {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            finishAffinity();
        }
    }

    @Override
    public void onNegativeClick(int from) {

    }

    @Override
    public void onNeutralClick(int from) {

    }

    @Override
    public void onBackPressed() {
//        FragmentManager fm = getSupportFragmentManager();
//        if (fm.getBackStackEntryCount() > 0) {
//            fm.popBackStack(2, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//        } else {
//            alertDialog.showAlertDialog("", "Are you sure you want to exit?", "Ok", "Cancel", 0, true);
//        }

        Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            alertDialog.showAlertDialog("", "Are you sure you want to exit?", "Ok", "Cancel", 0, true);
        }
    }

    @Override
    public void setSearchListener(String status, String searchString) {
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.intent_ComingFrom, AppConstants.intent_search);
        bundle.putString(AppConstants.intent_search_word, searchString);
        CarServicePointFragment mFragment = new CarServicePointFragment();
        mFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, mFragment).commit();

    }
}
