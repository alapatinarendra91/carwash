package com.carwash.ui.activiy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.carwash.MainActivity;
import com.carwash.R;
import com.carwash.constants.AppConstants;
import com.carwash.constants.ErrorModel;
import com.carwash.constants.Event;
import com.carwash.constants.VolleyErrorListener;
import com.carwash.constants.WebServices;
import com.carwash.model.CommonRootModel;
import com.carwash.model.RequestModel;
import com.carwash.utils.AlertDialogHelper;
import com.carwash.utils.CommonUtils;
import com.carwash.utils.PrefUtil;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import carwash.ext.GsonObjectRequest;
import carwash.ext.RequestManager;
import carwash.utils.ToastUtils;


public class LoginActivity extends CarWashBaseActivity implements View.OnClickListener, AlertDialogHelper.AlertDialogListener {

    private RelativeLayout mainLay;
    private EditText emailEdt, passwordEdt;
    private TextView forgotPasswordText,signupText;
    private Button loginBtn;

    private AlertDialogHelper alertDialog;
    private Context mContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initViews();
        setViews();

    }

    private void initViews() {
        mContext = this;
        alertDialog = new AlertDialogHelper(mContext);
        alertDialog.setOnAlertListener(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Login");
        setSupportActionBar(toolbar);

        mainLay = (RelativeLayout) findViewById(R.id.mainLay);
        emailEdt = (EditText) findViewById(R.id.emailEdt);
        passwordEdt = (EditText) findViewById(R.id.passwordEdt);
        forgotPasswordText = (TextView) findViewById(R.id.forgotPasswordText);
        loginBtn = (Button) findViewById(R.id.loginBtn);
        signupText = (TextView) findViewById(R.id.signupText);
    }

    private void setViews() {
        loginBtn.setOnClickListener(this);
        forgotPasswordText.setOnClickListener(this);
        signupText.setOnClickListener(this);

//        emailEdt.setText("alapatinarendra91@gmail.com");
//        passwordEdt.setText("1");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.loginBtn:
                if (emailEdt.getText().toString().equals("")) {
                    ToastUtils.showToast(mContext,"Please enter Email");
                    return;
                }

                if (passwordEdt.getText().toString().equals("")) {
                    ToastUtils.showToast(mContext,"Please enter password");
                    return;
                }

                getData(Event.customerlogin);

                break;

            case R.id.forgotPasswordText:
                if (emailEdt.getText().toString().equals("")) {
                    CommonUtils.showSnackBar(mainLay, "Please enter Email");
                    return;
                }
                alertDialog.showAlertDialog("Forgot Password", "Are you sure you want to get your password to "+ emailEdt.getText().toString() +"  email?", "Yes", "No", 0, true);
                break;

            case  R.id.signupText:
                startActivity(new Intent(mContext,RegistrationActivity.class));
                break;


        }
    }

    @Override
    public void getData(int actionID) {
        showProgressDialog(getString(R.string.please_wait), false);
        HashMap<String, String> reqHeader = new HashMap<>();
        reqHeader.put("Content-Type", "application/json; charset=utf-8");
        String request = "";
        Map<String,String> params = new HashMap<String, String>();
        switch (actionID) {
            case Event.customerlogin:
                request = sendLoginRequest();
                CommonUtils.logPrint("TAG", "request : " + request);
                RequestManager.addRequest(new GsonObjectRequest<CommonRootModel>
                        (WebServices.customerlogin, params, request, CommonRootModel.class,
                                new VolleyErrorListener(this, Event.customerlogin)) {
                    @Override
                    protected void deliverResponse(CommonRootModel response) {
                        updateUi(true, Event.customerlogin, response);
                    }
                });
                break;

            case Event.forgotpassword:
                request = sendForgotpasswordRequest();
                CommonUtils.logPrint("TAG", "request : " + request);
                RequestManager.addRequest(new GsonObjectRequest<CommonRootModel>
                        (WebServices.forgotpassword, params, request, CommonRootModel.class,
                                new VolleyErrorListener(this, Event.forgotpassword)) {
                    @Override
                    protected void deliverResponse(CommonRootModel response) {
                        updateUi(true, Event.forgotpassword, response);
                    }
                });
                break;

        }
    }

    @Override
    public void updateUi(boolean status, int action, Object serviceResponse) {
        removeProgressDialog();
        if (status) {
            switch (action) {
                case Event.customerlogin:
                    CommonRootModel mLoginModel = (CommonRootModel) serviceResponse;
                    CommonUtils.showToast(mContext, "" + mLoginModel.getStatusText());
                    if (mLoginModel.getStatusCode() == 200){
                        PrefUtil.putString(mContext, AppConstants.PREF_customer_id, mLoginModel.getData().getCustomerId(), AppConstants.PREF_NAME);
                        PrefUtil.putString(mContext, AppConstants.PREF_customer_email, mLoginModel.getData().getCustomerEmail(), AppConstants.PREF_NAME);
                        PrefUtil.putString(mContext, AppConstants.PREF_customer_Name, mLoginModel.getData().getCustomer_fname() + " " + mLoginModel.getData().getCustomer_lname(), AppConstants.PREF_NAME);
                        PrefUtil.putBool(mContext, AppConstants.PREF_firstTimeLogin, true, AppConstants.PREF_NAME);
                        startActivity(new Intent(mContext,HomeActivity.class));
                        finish();
                    }
                    break;

                case Event.forgotpassword:
                    CommonRootModel mForGotModel = (CommonRootModel) serviceResponse;
                    CommonUtils.showToast(mContext, "" + mForGotModel.getStatusText());
                    PrefUtil.putString(mContext, AppConstants.PREF_customer_id, mForGotModel.getCustomerId(), AppConstants.PREF_NAME);
                    startActivity(new Intent(mContext,ChangePasswordActivity.class).putExtra(AppConstants.intent_ComingFrom,AppConstants.intent_loginScreen));
                    break;

            }
        } else {
            ErrorModel response = (ErrorModel) serviceResponse;
            CommonUtils.showToast(mContext, "" + response.getStatusText());

        }
    }



    private String sendLoginRequest() {
        RequestModel mModel = new RequestModel();
        mModel.setMobileoremail(emailEdt.getText().toString());
        mModel.setPassword(passwordEdt.getText().toString());
        return new Gson().toJson(mModel);
    }

    private String sendForgotpasswordRequest() {
        RequestModel mModel = new RequestModel();
        mModel.setMobileoremail(emailEdt.getText().toString());
        return new Gson().toJson(mModel);
    }


    @Override
    public void onPositiveClick(int from) {
        getData(Event.forgotpassword);
    }

    @Override
    public void onNegativeClick(int from) {

    }

    @Override
    public void onNeutralClick(int from) {

    }
}
