package com.carwash.ui.adapter;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.carwash.R;
import com.carwash.model.CommonListModel;
import com.carwash.utils.CommonUtils;

import java.util.ArrayList;

/**
 * Created by CIS16 on 28-Dec-17.
 */

public class ServicePointsAdapter extends RecyclerView.Adapter<ServicePointsAdapter.MyHolder> {

    private Context mContext;
    private ArrayList<CommonListModel> mList;
    private OnCartChangedListener onCartChangedListener;
    private View view;

    /**
     * Constructor provides information about, and access to, a single constructor for a class.
     *
     * @param mContext
     * @param mList
     */
    public ServicePointsAdapter(Context mContext, ArrayList<CommonListModel> mList) {
        this.mContext = mContext;
        this.mList = mList;

    }

    @Override
    public ServicePointsAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.adapter_car_servicelist, null);
        ServicePointsAdapter.MyHolder myHolder = new ServicePointsAdapter.MyHolder(view);
        return myHolder;
    }

    @Override
    public void onBindViewHolder(final ServicePointsAdapter.MyHolder holder, final int position) {

        holder.textName.setText(CommonUtils.fromHtml("Name: ",mList.get(position).getShopName()));
        holder.textLocaton.setText(CommonUtils.fromHtml("Location: ",mList.get(position).getShopAddress()));
        holder.rating.setText(CommonUtils.fromHtml("Rating: ",mList.get(position).getShop_ratings()));

        holder.imagefav.setImageResource(mList.get(position).getIs_favourite() != null && mList.get(position).getIs_favourite() == 1 ? R.drawable.ic_fav : R.drawable.ic_unfav);

        holder.imagefav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCartChangedListener.setCartClickListener("imagefav", position);
            }
        });

        holder.convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCartChangedListener.setCartClickListener("convertView", position);
            }
        });


    }


    @Override
    public int getItemCount() {
        return (null != mList ? mList.size() : 0);
//        return 5;
    }


    public class MyHolder extends RecyclerView.ViewHolder {
        private ImageView imagefav;
        private TextView textName,textLocaton,rating;
        private View convertView;

        public MyHolder(View view) {
            super(view);
            convertView = view;
            imagefav= (ImageView)itemView.findViewById(R.id.imagefav);
            textName=itemView.findViewById(R.id.textname);
            textLocaton=itemView.findViewById(R.id.textLocation);
            rating=itemView.findViewById(R.id.textrating);
        }
    }


    public void setOnCartChangedListener(OnCartChangedListener onCartChangedListener) {
        this.onCartChangedListener = onCartChangedListener;
    }

    public interface OnCartChangedListener {
        void setCartClickListener(String status, int position);
    }


}