package com.carwash.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.carwash.R;

/**
 * Created by i5 on 01-10-2018.
 */

public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.MyHolder> {

    private Context mContext;
    //    private ArrayList<SearchJobsModel> mSearchJobsModelList;
    private MyOrdersAdapter.OnCartChangedListener onCartChangedListener;
    private View view;

    /**
     * Constructor provides information about, and access to, a single constructor for a class.
     *
     * @param mContext
     * @param mSearchJobsModelList
     */
    public MyOrdersAdapter(Context mContext) {
        this.mContext = mContext;
//        this.mSearchJobsModelList = mSearchJobsModelList;

    }

    @Override
    public MyOrdersAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.adapter_ordres, null);
        MyOrdersAdapter.MyHolder myHolder = new MyOrdersAdapter.MyHolder(view);
        return myHolder;
    }

    @Override
    public void onBindViewHolder(final MyOrdersAdapter.MyHolder holder, final int position) {


//        holder.itemNumText.setText(mSearchJobsModelList.get(position).getItemNumber());


        holder.convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCartChangedListener.setCartClickListener("convertView", position);
            }
        });


    }


    @Override
    public int getItemCount() {
//        return (null != mSearchJobsModelList ? mSearchJobsModelList.size() : 0);
        return 5;
    }


    public class MyHolder extends RecyclerView.ViewHolder {
        private View convertView;

        public MyHolder(View view) {
            super(view);
            convertView = view;
        }
    }


    public void setOnCartChangedListener(OnCartChangedListener onCartChangedListener) {
        this.onCartChangedListener = onCartChangedListener;
    }

    public interface OnCartChangedListener {
        void setCartClickListener(String status, int position);
    }


}